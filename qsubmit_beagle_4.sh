#!/bin/bash

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle

if [ ! -d "eo" ]; then
 mkdir eo
fi
if [ ! -d "conformOutput" ]; then 
 mkdir conformOutput
fi
if [ ! -d "beagleOutput" ]; then 
 mkdir beagleOutput
fi 

# prefix of the file names, typically derived from, e.g.,  original/${prefix}.bed 
prefix=$1 
# file that lists samples to exclude 
removefile=$2
qsubresources=$3

for chrom in `cat ${scriptdir}/chromosome_arms.txt`
do 
 echo $chrom
 qsub -l $qsubresources -d . -e eo -o eo -N beascript_${chrom} \
   -v dir=".",prefix="${prefix}_imputation",removefile="${removefile}",chrom="${chrom}",conformprefix="conformOutput/${prefix}_conform",beagleoutputfile="beagleOutput/${prefix}_beagle" ${scriptdir}/submit_beagle_4.sh 
done 










#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################