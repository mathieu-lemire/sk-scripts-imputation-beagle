#######################################################

Scripts used here are located in 

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle

Current version is v0.1.5 (last git tag is v0.1.4)

based on commit 

# copy the output of 
# cd $scriptdir
# git log -n 1

commit 906c63a53e7cd68c7bccabe96267af53a099f3c5
Author: Mathieu Lemire <mathieu.lemire@sickkids.ca>
Date:   Fri May 3 11:09:32 2019 -0400

    changed how the range was extended to make sure the vcf overlap is not empty. based on conform PASS SNPs



#######################################################


######################################################
# Create a directory where the imputation will be done
######################################################

/hpf/projects/arnold/data/genotypes/<projectname>/imputation/beagle/v0.X

# and switch to it

################################
# Create the directory structure
################################
if [ ! -d "eo" ]; then
 mkdir eo
fi
if [ ! -d "conformOutput" ]; then 
 mkdir conformOutput
fi
if [ ! -d "beagleOutput" ]; then 
 mkdir beagleOutput
fi 

#######################
# define some variables
#######################

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle

# this is where the base plink files are to be found, before qc
dir=../../../recoded 
# prefix is the prefix of the plink files, such as in ${dir}/${prefix}.bed  
prefix=
# qc version. e.g. qcversion=v0.2.0
qcversion=

# excludefile is a list of SNPs to exclude, based on QC
# will typically be ../../../qc/v0.X/${prefix}.exclude.txt 
# dont write "current" instead of v0.x for traceability purposes
excludefile=../../../qc/${qcversion}/${prefix}.exclude.txt 
 echo === head of plink exclude file below ===
 head $excludefile 
 echo ========================================

removefile=../../../qc/${qcversion}/${prefix}.remove.txt 
 echo === head of plink remove file below  ===
 head $removefile 
 echo ========================================

# this is the name of the plink files to use for impuation purposes
# in there markers are excluded based on qc and renamed 
outputprefix=${prefix}_imputation

##################################################################
# Rename the SNPs to the ones found in the 1000 genomes references
##################################################################

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle

${scriptdir}/recode_snp_names.sh $dir $prefix $excludefile $outputprefix > eo/recode_snp_names.o 2> eo/recode_snp_names.e

# or via qsub

# Edit the qsub resources here 
qsubresources="vmem=4G,walltime=02:00:00"

qsub -d . -l ${qsubresources} -N recode_snp_names -e eo -o eo \
  -v dir="$dir",prefix="$prefix",excludefile="$excludefile",outputprefix="$outputprefix" ${scriptdir}/recode_snp_names.sh



#######################################
# run the imputation. this uses 4 cores and 
#######################################

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle


# after git tag v0.1, I now exclude sex problems from the remove file 
awk '$3!="SEXCHECK" {print}' $removefile > _removefile

# Edit the qsub resources here 
qsubresources="vmem=20g,walltime=16:00:00,nodes=1:ppn=4"

${scriptdir}/qsubmit_beagle_4.sh $prefix _removefile $qsubresources


#######################################
# merge chromosomes 
#######################################


scriptdir=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle

cd beagleOutput

qsubresources="vmem=8g,walltime=4:00:00"

# listing all chromosome values 
chroms=`ls -1 *vcf.gz | grep ":" | cut -f 1 -d':' | rev | cut -f 1 -d'_' | rev | sort -u`
# I am sending to pbs input files separated by ";"  
for chr in $chroms; do 
 
  vcffiles=`ls *_${chr}:*.vcf.gz`
  vcffiles=`echo $vcffiles | sed 's/ /;/g'`
  output=${prefix}_beagle_chr${chr}.vcf.gz 
  
  qsub -d . -l ${qsubresources} -N merge_by_chr_${chr} -e ../eo -o ../eo \
    -v vcffiles="$vcffiles",output="$output" ${scriptdir}/merge_by_chr.sh

done 



#######################################
# validate output -- compare with 1kg
# and look for missing markers.
#######################################

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle

cd beagleOutput

qsubresources="vmem=8g,walltime=4:00:00"

# listing all chromosome values 
chroms=`ls -1 *chr*vcf.gz | cut -f 1 -d'.' | rev | cut -f 1 -d'_' | rev | sed 's/chr//g'`

for chr in $chroms; do 
 
  vcffile=${prefix}_beagle_chr${chr}.vcf.gz 
  
  qsub -d . -l ${qsubresources} -N validate_vcf_${chr} -e ../eo -o ../eo \
    -v chr="$chr",vcffile="$vcffile" ${scriptdir}/validate_vcf.sh 

done 

#######################################
# dealing with MISSING snps
#######################################

# if there is a ${prefix}_beagle_chr${chr}.vcf.gz.MISSING file, 
# then edit and run file fix_missing_snps.sh for each chr

# if the script fix_missing_snps.sh is not found in current 
# directory then the following steps were not applied.

### cp  ~/mlemire/scripts/imputation/beagle/fix_missing_snps.sh  .

# edit appropriately 

# qsubresources="vmem=20g,walltime=8:00:00,nodes=1:ppn=1"

# qsub -d .  -l $qsubresources -N fix_missing_snps -e eo -o eo ./fix_missing_snps.sh


########################################
# merge into the chr12.vcf.gz file 
# THOSE ARE ILLUSTRATIONS ONLY 
########################################

# scriptdir=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle

# qsubresources="vmem=8g,walltime=4:00:00"

# cd beagleOutput 

# qsub -d . -l ${qsubresources} -N merge_12_MISSING -e ../eo -o ../eo \
#    -v vcffiles="crosbie_qc0_beagle_chr12.vcf.gz;crosbie_qc0_beagle_12:11707578-34906670_MISSING.vcf.gz",output=tmp_crosbie_qc0_beagle_chr12.vcf.gz ${scriptdir}/merge_by_chr.sh

# inspect file (validate) tmp_crosbie_qc0_beagle_chr12.vcf.gz  then if ok 
# mv tmp_crosbie_qc0_beagle_chr12.vcf.gz  crosbie_qc0_beagle_chr12.vcf.gz 
# mv tmp_crosbie_qc0_beagle_chr12.vcf.gz.tbi  crosbie_qc0_beagle_chr12.vcf.gz.tbi


#chr=12
#vcffile=tmp_crosbie_qc0_beagle_chr12.vcf.gz
#qsub -d . -l ${qsubresources} -N validate_tmp_crosbie_qc0_beagle_chr12 -e ../eo -o ../eo \
#    -v chr="$chr",vcffile="$vcffile" ${scriptdir}/validate_vcf.sh 


#######################################
# cleaning up
#######################################

\rm *_[0-9]:[0-9]*.vcf.gz*
\rm *_[0-9][0-9]:[0-9]*.vcf.gz*


######################################
# creating files that contains INFO 
######################################

cd beagleOutput 

module load bcftools
vcfs=`ls *vcf.gz`
for vcf in $vcfs; do 
 echo $vcf
 prefix=`echo $vcf | sed 's/.vcf.gz//g'`
 zcat $vcf | cut -f -8 > ${prefix}.INFO.vcf
done 














#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################