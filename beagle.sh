#!/bin/bash 

module load plink/1.9.b3.42
module load java

jar=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle/beagle.27Jan18.7e1.jar 

conformprefix=$1
chrom=$2
outputfile=$3
nthreads=$4

#conformprefix=_194766_test
#chrom=22:26544582-27544582
#outputfile=_$$_test_beagle_script 
#nthreads=1

chr=`echo $chrom |cut -f 1 -d':'`
from=`echo $chrom |cut -f 2 -d':' | cut -f 1 -d'-'`
to=`echo $chrom |cut -f 2 -d':' | cut -f 2 -d'-'`

# figuring out the actual from-to
# based on extension made in conform.sh

# MOD v0.1.4 
actualfrom=`grep PASS ${conformprefix}.log | head -1 | awk '{print $2}'`
actualto=`grep PASS ${conformprefix}.log | tail -1 | awk '{print $2}'`

echo $from $to  $actualfrom  $actualto 

extended=0 
# conform.sh possibly extended the range so that it includes a minimum number of SNPs. 
# boundaries were extended by +/- 500kb until the number of snps was as required 
while [ $actualfrom -lt  $from   ] || [ $actualto -gt $to ]; do 

  extended=1

  echo =======================  
  echo EXTENDING RANGE 

  from=`echo $from | awk '{$1=$1-500000}{$1<1?$1=1:$1=$1}{print $1}'`
  to=`echo $to | awk '{$1=$1+500000}{print $1}'`
  echo $from $to  $actualfrom  $actualto 

  echo =======================  

done 


tmpfile=_$$_tmpfile_

ref=/hpf/projects/arnold/references/bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/b37.vcf/chr${chr}.1kg.phase3.v5a.vcf.gz

time java -Xss5m -Xmx8g -jar $jar ref=$ref gt=${conformprefix}.vcf.gz   chrom=${chr}:${from}-${to}  out=${outputfile} gprobs=true nthreads=${nthreads}

# if the range was extended, then I get remove all that wasnt' in the original range 
if [ $extended = 1 ]; then 

  echo removing SNPs not in original range 
  module load bedtools/2.27.1
  module load tabix 
  echo $chrom | sed 's/[:-]/\t/g' > ${tmpfile}_bed 
  bedtools intersect -header -wa -a ${outputfile}.vcf.gz -b  ${tmpfile}_bed  > ${tmpfile}.vcf 
  bgzip ${tmpfile}.vcf 
  mv  ${tmpfile}.vcf.gz  ${outputfile}.vcf.gz
  \rm ${tmpfile}* 

fi 




\rm ${tmpfile}*











#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################
