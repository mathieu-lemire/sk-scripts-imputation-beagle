#!/bin/bash


#dir=$1
#prefix=$2
#removefile=$3
#chrom=$4 
#conformprefix=$5
#beagleoutputfile=$6

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle


echo =========================
echo ARGUMENTS:
echo $dir
echo $prefix
echo $removefile
echo $chrom
echo $conformprefix
echo $beagleoutputfile
echo =========================

echo Running confirm...
${scriptdir}/conform.sh $dir $prefix $removefile $chrom ${conformprefix}_${chrom} 
echo .. done. Output files are:
ls ${conformprefix}_${chrom}*

echo Running beagle ...
${scriptdir}/beagle.sh ${conformprefix}_${chrom}  $chrom ${beagleoutputfile}_${chrom}  4 
echo ... done.  Output files are:
ls ${beagleoutputfile}_${chrom}*














#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################