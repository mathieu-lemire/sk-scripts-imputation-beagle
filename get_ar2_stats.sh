#!/bin/bash

if [ -z $vcf ]; then
 vcf=$1
fi

output=`echo $vcf | rev | cut -f 1 -d'/' | rev |awk '{print $1".ar2stats.txt"}'`
module load vcflib 

echo AR2 N > $output 
zcat $vcf | grep -v "#" | wc -l | awk '{print "all",$1}' >> $output 
vcffilter -f "AR2 > 0.30" $vcf | wc -l | awk '{print "0.30",$1}' >> $output
vcffilter -f "AR2 > 0.50" $vcf | wc -l | awk '{print "0.50",$1}' >> $output
vcffilter -f "AR2 > 0.80" $vcf | wc -l | awk '{print "0.80",$1}' >> $output
vcffilter -f "AR2 > 0.90" $vcf | wc -l | awk '{print "0.90",$1}' >> $output
vcffilter -f "AR2 > 0.99" $vcf | wc -l | awk '{print "0.99",$1}' >> $output










#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################