#!/bin/bash

module load plink/1.07

##############
# arguments 

# tests if the variable is defined, through, e.g. qsub -v 
if [ -z "$dir" ]; then
 dir=$1
 # SHA15345
 prefix=$2
 #SNPs to exclude due to qc. this includes duplicates 
 # ../../../qc/v0.1/SHA15345.exclude.txt
 excludefile=$3
 # prefix of plink files 
 outputprefix=$4
 
fi 


echo =========================
echo ARGUMENTS:
echo $dir
echo $prefix
echo $excludefile
echo $outputprefix
echo =========================




##############
# this lists the SNP names found in the references with chr:pos:al1:al2
# e.g.
#rs10 7:92383888:A:C
#rs10000 7:6013153:G:A
#rs1000000 12:126890980:A:G
#rs10000000 4:40088896:T:A
#rs10000003 4:57561647:A:G

######################
# hard coded variables

namekey=/hpf/projects/arnold/references/bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/b37.vcf/snpnames_key_v2.txt
tmpfile=_$$_tmpfile_


######################
#


# excluding SNPs monomorphic or indels 
# EXCLUDING AMBIGUOUS AS OF v0.1.1
awk '$5==0 || $5=="I" || $5=="D" || ($5=="A" && $6=="T" ) || ($5=="T" && $6=="A" ) || ($5=="G" && $6=="C" ) || ($5=="C" && $6=="G" ) {print $2}' ${dir}/${prefix}.bim  > ${tmpfile}_exclude
cat $excludefile >> ${tmpfile}_exclude 

plink --bfile  ${dir}/${prefix}  --exclude ${tmpfile}_exclude  --make-bed --out ${tmpfile}

awk '{print $1":"$4,$0,NR}' ${tmpfile}.bim | sort > ${tmpfile}_bim 

# matching position and both alleles 

# in there there are multiples of chr:pos that can be found 
join -1 1 -2 1 $namekey ${tmpfile}_bim > ${tmpfile}_match_pos 

join -v 2 -1 1 -2 1 $namekey ${tmpfile}_bim   > ${tmpfile}_nomatch_pos 

cat ${tmpfile}_match_pos |\
 awk 'BEGIN{s["A"]="T";s["C"]="G";s["G"]="C";s["T"]="A"}
      $2==$9 && $3==$10 || $2==$10 && $3==$9 || $2==s[$9] && $3==s[$10] || $2==s[$10] && $3==s[$9] {print}' > ${tmpfile}_match_pos_alleles 

# do not match alleles 
# there are multiples of chr:pos in there, because of ${tmpfile}_match_pos  . Need to do uniq below
join -v 1 -1 1 -2 1 ${tmpfile}_match_pos ${tmpfile}_match_pos_alleles > ${tmpfile}_match_pos_no_match_alleles


awk '{print $5,$4,$7,$8,$9,$10,$11}' ${tmpfile}_match_pos_alleles  > ${tmpfile}_bim
# see above about uniq . also there was a bu there $4 was in the output instead of $6
awk '{print $5,$6,$7,$8,$9,$10,$11}' ${tmpfile}_match_pos_no_match_alleles | uniq  >> ${tmpfile}_bim 
awk '{print $4}' ${tmpfile}_match_pos_no_match_alleles > ${outputprefix}_exclude_no_match_alleles.txt
cut -f 2- -d' '  ${tmpfile}_nomatch_pos  >>  ${tmpfile}_bim 

#checking
echo \################################################################################
echo \################################################################################
echo 
echo You should see no lines of output
echo ============ between this line
sort -k 7,7 -g ${tmpfile}_bim  | awk 'NR!=$7 {print}' | head 
echo ============ and this line 
echo 
echo \################################################################################
echo \################################################################################
# emtpy, all are accounted for
sort -k 7,7 -g ${tmpfile}_bim  | cut -f -6 -d' ' | sed 's/ /\t/g' > ${tmpfile}_recoded.bim 


## 

cat  ${outputprefix}_exclude_no_match_alleles.txt >> ${tmpfile}_exclude 
plink --bed ${tmpfile}.bed --fam ${tmpfile}.fam --bim ${tmpfile}_recoded.bim \
 --exclude ${tmpfile}_exclude --make-bed --out $outputprefix 

\rm ${tmpfile}*









#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################