#!/bin/bash 


if [ -z "$chr" ]; then 
 chr=$1
 vcffile=$2 
fi

module load bcftools/1.6

echo USING PROCESS ID $$ 

ref=/hpf/projects/arnold/references/bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/b37.vcf/chr${chr}.1kg.phase3.v5a.vcf.gz

bcftools query -f "%CHROM:%POS:%REF:%ALT\n" $ref | sort > $$_chrpos.1kg 
bcftools query -f "%CHROM:%POS:%REF:%ALT\n" $vcffile | sort > $$_chrpos.input

join -v 2 -1 1 -2 1 $$_chrpos.input $$_chrpos.1kg > $$_missing

\rm  $$_chrpos.input $$_chrpos.1kg 

n=`wc -l $$_missing |awk '{print $1}'`
if [ $n -gt 0 ]; then 
 mv $$_missing ${vcffile}.MISSING
else
 echo EMPTY ${vcffile}.MISSING 
 \rm  $$_missing 
fi 



# creating smaller file that contains the INFO

prefix=`echo $vcffile | sed 's/.vcf.gz//g'`
zcat $vcffile | cut -f -8 > ${prefix}.INFO.vcf









#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################