#!/bin/bash 

# steps to take in case an impuation vcf is MISSING some SNPs

scriptdir=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle

# where the plink files live, the ones created by recode_snp_names.sh  
# typically the current directory 
dir=.
# usual prefix of the plink files
prefix=
# chromosome that has missing SNPs
chr=
# samples to remove MAKE SURE IT MATCHES THE ONE FOR MAIN IMPUTATION
removefile=

# vcf file that has missing SNPs
vcffile=beagleOutput/${prefix}_beagle_chr${chr}.vcf.gz 
missing=${vcffile}.MISSING

# extending the range 
from=`cut -f 2 -d':' $missing | sort -g | head -1 | awk '{print $1-50000}'`
to=`cut -f 2 -d':' $missing | sort -g | tail -1 |awk '{print $1+50000}'`
chrom=${chr}:${from}-${to}

conformoutput=conformOutput/${prefix}_conform_${chrom}_MISSING


${scriptdir}/conform.sh $dir ${prefix}_imputation $removefile $chrom $conformoutput 


# beagle will fail if can't find genotyped markers in a given 
# window.  Extending the window size may solve such problems
# default window is 50000 

module load java

jar=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle/beagle.27Jan18.7e1.jar 


beagleoutput=beagleOutput/${prefix}_beagle_${chrom}_MISSING
nthreads=4


ref=/hpf/projects/arnold/references/bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/b37.vcf/chr${chr}.1kg.phase3.v5a.vcf.gz

time java -Xss5m -Xmx8g -jar $jar window=100000 ref=$ref gt=${conformoutput}.vcf.gz   chrom=${chrom} out=${beagleoutput} gprobs=true nthreads=${nthreads}












#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################