#!/bin/bash

module load bcftools/1.6
module load tabix 

echo USING PROCESS ID $$ 


# needs a list of vcf files as arguments. at least two. 
# the first one will be merged with the second one, then
# the result will be merged with thirds, and so on. 
#
# this is to deal with possibly overlapping SNPs. In which
# case I pick the entry that has the highest AR2 or the one 
# from the merged file (first file) in case of tie

# if called as bash script then last arguemnt is output 
if [ -z "$vcffiles" ]; then 

 vcffiles=( "$@" )
 argc=${#vcffiles[@]}
 output=${vcffiles[$argc-1]}
 # argc is now the number of input vcf files to merge  
 argc=$[$argc-1] 

else 

 # called as pbs script, in which case the input file list 
 # is given as semi-column-delimited (I'm having a problem with passing commas)
 # and output given on its own
 # turning vcffiles into an array

 # qsub -v vcffiles="file1;file2",output="output" 

 vcffiles=(`echo $vcffiles | sed 's/;/ /g'`)
 argc=${#vcffiles[@]}

fi

#for i in `seq 1 $argc`; do 
# echo using vcf input ${vcffiles[$i-1]}
#done
#echo writing to file $output



##################################
# ONE INPUT FILE 
##################################
# I am allowing only 1 file as input, in which case it is copied to the output 

if [ $argc -eq 1 ]; then 
  
 if [ ! -e ${vcffiles[0]}.tbi ]; then 
  echo indexing ${vcffiles[0]} ...
  tabix -p vcf  ${vcffiles[0]}
  echo ... done
 fi
 
 cp ${vcffiles[0]}  ${output} 
 cp ${vcffiles[0]}.tbi  ${output}.tbi 

 exit 0 

fi 




##################################
# TWO OR MORE INPUT FILES 
##################################

# temp dir where bcftool isec does its work 
isecdir=$$_isec

# indexing if necessary
if [ ! -e ${vcffiles[0]}.tbi ]; then 
 echo indexing ${vcffiles[0]} ...
 tabix -p vcf  ${vcffiles[0]}
 echo ... done
fi

if [ ! -e ${vcffiles[1]}.tbi ]; then 
 echo indexing ${vcffiles[1]} ...
 tabix -p vcf  ${vcffiles[1]}
 echo ... done 
fi


echo intersecting ${vcffiles[0]} with ${vcffiles[1]}
echo calling bcftools isec ...

bcftools isec -p $isecdir -O z  ${vcffiles[0]} ${vcffiles[1]}

echo ... done 

echo merging private SNPs from ${vcffiles[0]} with ${vcffiles[1]} ...

bcftools concat -o $$_private.vcf.gz  -O z ${isecdir}/0000.vcf.gz  ${isecdir}/0001.vcf.gz  

echo ... done 

echo dealing with overlapping SNPs ...

# no need for \n when using %LINE as there is a newline there
bcftools query -o $$_ar2_0002 -f "%INFO/AR2@%LINE" ${isecdir}/0002.vcf.gz
bcftools query -o $$_ar2_0003 -f "%INFO/AR2@%LINE" ${isecdir}/0003.vcf.gz

bcftools view -h ${isecdir}/0000.vcf.gz >  $$_overlap.vcf 

# the queries above and the paste use @ as separator. The two AR2 to compare are thus $1 and $3
paste -d "@" $$_ar2_0002 $$_ar2_0003 |\
 awk 'BEGIN{FS="@"}{if( $3>$1 ){ print $4} else {print $2}}' >> $$_overlap.vcf 

\rm -rf $isecdir/
\rm -rf $$_ar2_0002 $$_ar2_0003

echo ... done 

echo merging private and ovelap ...

bcftools concat -o $$_all.vcf.gz  -O z $$_private.vcf.gz   $$_overlap.vcf  

\rm  $$_private.vcf.gz   $$_overlap.vcf  

echo ... done
echo sorting vcf ...

bcftools sort -O z -o $$_sorted.vcf.gz $$_all.vcf.gz 
tabix -p vcf  $$_sorted.vcf.gz 
\rm  $$_all.vcf.gz 

echo ... done 

# dealing with addition files 
if [ $argc -gt 2 ]; then 

 for i in `seq 3 $argc`; do
    
  if [ ! -e ${vcffiles[$i-1]}.tbi ]; then 
   echo indexing ${vcffiles[$i-1]} ...
   tabix -p vcf  ${vcffiles[$i-1]}
   echo ... done 
  fi

  echo intersecting $$_sorted.vcf.gz with ${vcffiles[$i-1]}
  echo calling bcftools isec ...

  bcftools isec -p $isecdir -O z $$_sorted.vcf.gz ${vcffiles[$i-1]}

  echo ... done 

  echo merging private SNPs from $$_sorted.vcf.gz with ${vcffiles[$i-1]} ...

  bcftools concat -o $$_private.vcf.gz  -O z ${isecdir}/0000.vcf.gz  ${isecdir}/0001.vcf.gz  

  echo ... done 

  echo dealing with overlapping SNPs ...

  bcftools query -o $$_ar2_0002 -f "%INFO/AR2@%LINE" ${isecdir}/0002.vcf.gz
  bcftools query -o $$_ar2_0003 -f "%INFO/AR2@%LINE" ${isecdir}/0003.vcf.gz

  bcftools view -h ${isecdir}/0000.vcf.gz >  $$_overlap.vcf 
  paste -d "@" $$_ar2_0002 $$_ar2_0003 |\
   awk 'BEGIN{FS="@"}{if( $3>$1 ){ print $4} else {print $2}}' >> $$_overlap.vcf 

  \rm -rf $isecdir/
  \rm -rf $$_ar2_0002 $$_ar2_0003

  echo ... done

  echo merging private and ovelap ...

  bcftools concat -o $$_all.vcf.gz  -O z $$_private.vcf.gz   $$_overlap.vcf  

  \rm  $$_private.vcf.gz   $$_overlap.vcf  

  echo ... done
  echo sorting vcf ...

  bcftools sort -O z -o $$_sorted.vcf.gz $$_all.vcf.gz 
  tabix -p vcf  $$_sorted.vcf.gz 
  \rm  $$_all.vcf.gz 

  echo ... done 

 done

fi

echo writing to file $output ... 

mv  $$_sorted.vcf.gz  ${output}
mv  $$_sorted.vcf.gz.tbi  ${output}.tbi 

# don't have to clean anything else 
# \rm $$_* 


echo ... all done

# cd /hpf/projects/arnold/users/mlemire/scripts/imputation/beagle/tests/merge_by_chr
#  ../../merge_by_chr.sh 1.vcf.gz 2.vcf.gz 3.vcf.gz test1.output.vcf.gz 
#  ../../merge_by_chr.sh 1.vcf.gz 3.vcf.gz 2.vcf.gz test2.output.vcf.gz 
# same result, except for header 









#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################