#!/bin/bash 

module load plink/1.9.b3.42
module load java/1.8.0_91

jar=/hpf/projects/arnold/users/mlemire/scripts/imputation/beagle/conform-gt.24May16.cee.jar

dir=$1
prefix=$2
removefile=$3
chrom=$4 
outputfile=$5

# dir=.
#prefix=SHA15345_imputation
#removefile=../../../qc/v0.1/SHA15345.remove.txt 
#chrom=22:26544582-27544582
#outputfile=_$$_test 


chr=`echo $chrom |cut -f 1 -d':'`
from=`echo $chrom |cut -f 2 -d':' | cut -f 1 -d'-'`
to=`echo $chrom |cut -f 2 -d':' | cut -f 2 -d'-'`
tmpfile=_$$_tmpfile_


if [ $chr != 23 ]; then 

 # Note that this assumes SNPs have been excluded already from  recode_snp_names.sh  
 plink --bfile ${dir}/${prefix} \
  --chr $chr  \
  --remove $removefile  \
  --recode vcf --out ${tmpfile}

elif [ $chr == 23 ]; then 

  plink --bfile ${dir}/${prefix} \
  --chr $chr  \
  --remove $removefile  \
  --recode vcf --out ${tmpfile} --set-hh-missing

  # plink marks males as 0 instead of 0/0
  # this is a workaround 
  
  # by doing this the sex info is lost (sex marked as 0 in fam) and all samples are then recoded as 0/0 etc 
  plink --vcf ${tmpfile}.vcf --make-bed --out ${tmpfile}
  plink --bfile  ${tmpfile}  --recode vcf --out ${tmpfile} 

fi 





npass=0
ref=/hpf/projects/arnold/references/bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/b37.vcf/chr${chr}.1kg.phase3.v5a.vcf.gz

counter=1 
# workaround if number of SNPs is too low to produce results 
# v0.1.5 changed from 50 to 1000 
while [ $npass -lt 1000 ] && [ $counter -lt  50 ]; do 
  \rm ${outputfile}.*
  counter=$[$counter+1]
  echo =======================
  echo extracting in ${chr}:${from}-${to}

  time java -Xss5m -Xmx8g -jar $jar  ref=${ref} gt=${tmpfile}.vcf  chrom=${chr}:${from}-${to} out=${outputfile}  strict=false 
  npass=`grep PASS ${outputfile}.log | wc -l | awk '{print $1}'`
  from=`echo $from | awk '{$1=$1-500000}{$1<1?$1=1:$1=$1}{print $1}'`
  to=`echo $to | awk '{$1=$1+500000}{print $1}'`
 
  echo npass: $npass
  echo =======================  

done 

\rm ${tmpfile}*











#############################################################################
#   Copyright 2019 Mathieu Lemire
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#############################################################################
